<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace think\app;

use think\Service as BaseService;

class Service extends BaseService
{
    public function boot()
    {
        $this->app->event->listen('HttpRun', function () {
            $this->app->middleware->add(ModuleApp::class);
        });

        $this->commands([
            'prod' => command\Prod::class,
            'build' => command\Build::class,
            'clear' => command\Clear::class,
        ]);

        $this->app->bind([
            'think\route\Url' => Url::class,
        ]);
        
        // 插件已通过多应用实现(以下是通过路由实现插件)
        // $this->registerRoutes(function (Route $route) {
        //    $handle = '\\think\\app\\Service::routeHandle';
        //    $route->rule("addons/:addon/[:controller]/[:action]", $handle); //->middleware(Addons::class);
        // });
    }

    /*
    public static function routeHandle($addon = null, $controller = null, $action = null)
    {
        $class = Service::getAddonPath($addon, $controller);
        if ($class == null) {
            throw new HttpException(404, lang('Addon %s not found!', [$addon]));
        }
        $call = [];
        $vars = [];
        $action = !empty($action) ? $action : 'index';
        $instance = new $class($addon, $controller, $action, app());
        if (is_callable([$instance, $action])) {
            $call = [$instance, $action];
            //Container::getInstance()->invokeMethod($call, $vars);
            return call_user_func_array($call, $vars);
        } else {
            throw new HttpException(404, lang('addon %s not found', [get_class($instance) . '->' . $action . '()']));
        }
    }

    public static function getAddonPath($name, $class = null, $type = 'hook')
    {
        $name = trim($name);
        if (!empty($class)) {
            $class = Str::studly($class);
            $namespace = '\\addons\\' . $name . '\\' . $class.'Addon'; //controller
        } else {
            $namespace = '\\addons\\' . $name . '\\IndexAddon';
        }
        //echo $namespace;exit;
        return class_exists($namespace) ? $namespace : null;
    }*/
}
